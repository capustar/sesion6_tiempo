//
//  ViewController.swift
//  sesion6_tiempo
//
//  Created by mastermoviles on 26/10/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var image_tiempo: UIImageView!
    
    @IBOutlet weak var label_tiempo: UILabel!
    
    @IBOutlet weak var edit_localidad: UITextField!
    
    let OW_URL_BASE = "https://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&appid=1adb13e22f23c3de1ca37f3be90763a9&q="
    let OW_URL_BASE_ICON = "https://openweathermap.org/img/w/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edit_localidad.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func consultarTiempo(localidad:String)
    {
        let urlString = OW_URL_BASE+localidad
        let url = URL(string:urlString)
        let dataTask = URLSession.shared.dataTask(with: url!)
        {
            datos, respuesta, error in
            if let jsonStd = try? JSONSerialization.jsonObject(with: datos!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
            {
                let weather = jsonStd["weather"]! as! [AnyObject]
                let currentWeather = weather[0] as! [String:AnyObject]
                let descripcion = currentWeather["description"]! as! String
                print("El tiempo en \(localidad) es: \(descripcion)")
                //Estamos bajándonos la imagen pero todavía no la usamos
                let icono = currentWeather["icon"]! as! String
                if let urlIcono = URL(string: self.OW_URL_BASE_ICON+icono+".png" )
                {
                    if let datosIcono = try? Data(contentsOf: urlIcono)
                    {
                        let imagenIcono = UIImage(data: datosIcono)
                        
                        OperationQueue.main.addOperation()
                        {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.label_tiempo.text = descripcion
                            self.image_tiempo.image = imagenIcono
                        }
                    }
                    else
                    {
                        print("Ha ocurrido un error con datos icono")
                    }
                }
            }
            else
            {
                print("Ha ocurrido un error con el json")
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        dataTask.resume()
    }
    
    @IBAction func button_consultar(_ sender: Any)
    {
        consultarTiempo(localidad : edit_localidad.text!)
    }
    
    func esTexto(cadena: String) -> Bool
    {
        if (Int(cadena) == nil)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return esTexto(cadena: string);
    }
}

